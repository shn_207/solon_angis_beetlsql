<div align="center">
    <p align="center">
        <img src="https://gitee.com/smartcity/images/raw/master/logo_union.jpg" height="150" alt="logo"/>
    </p>
</div>

## 框架介绍
并元国产开发平台 是基于Solon框架的国产快速开发平台，让你体验更快、更小、更少、更自由！！！启动快。

一键生成前后台代码，菜单模块SQL，轻松Ctrl+C、Ctrl+V

采用 Solon + beetlsql + SaToken + smart-http 等优秀组件及前沿技术开发，注释丰富，代码简洁，开箱即用！

Solon：更现代感的国产应用开发框架。更快、更小、更少、更自由！！！启动快 5 ～ 10 倍；qps 高 2～ 3 倍；运行时内存节省 1/3 ~ 1/2；打包可以缩到 1/2 ~ 1/10，目前Solon的生态越来越完善，欢迎来玩

欢迎加入QQ技术群互相解决问题：760590551

## 前端下载（solon_angis_vue3）：https://gitee.com/smartcity/solon_angis_vue3

## 演示地址：http://solon.aipix.cn

## 前端支撑
插件	版本	用途
node.js	最新版	JavaScript运行环境

## 后端支撑
| 插件 |	版本 |	用途 |
| --- |---------------|  ----- |
| jdk |	11	| java环境 |
| maven	| 最新版	| 包管理工具 |
| redis	| 最新版	| 缓存库 |
| mysql	| 8.0	| 数据库 |

## 启动前端
```
安装pnpm
pnpm i
pnpm run dev
```

## 启动后端
开发工具内配置好maven并在代码中配置数据库即可启动

## 代码结构
solon_angis 框架对代码以插件化的模式进行分包

```
solon_angis
  |-common == 基础通用模块
  |-db == 数据库基础模块
  |-generate == 代码生成模块
  |-system == 权限模块
  |-web == 主启动模块
```

## 效果图:
![输入图片说明](https://gitee.com/smartcity/images/raw/master/union_login.png)
![输入图片说明](https://gitee.com/smartcity/images/raw/master/union_menu.png)
	
	
	
## 感谢
- <a href="https://gitee.com/noear/solon" target="_blank">solon</a>
- <a href="https://gitee.com/dromara/sa-token" target="_blank">sa-token</a>
- <a href="https://gitee.com/xiandafu/beetlsql" target="_blank">beetlsql</a>
- <a href="https://gitee.com/smartboot/smart-http" target="_blank">smart-http</a>
- <a href="https://gitee.com/lyt-top/vue-next-admin" target="_blank">vue-admin-next</a>

## 支持作者

如果觉得框架不错，或者已经在使用了，希望你可以去 <a target="_blank" href="https://gitee.com/smartcity/solon_angis_beetlsql">Gitee</a> 帮我点个 ⭐ Star，这将是对我极大的鼓励与支持。

## 版权说明

代码可用于个人项目等接私活或企业项目脚手架使用，并元国产开发平台 开源版完全免费

二次开发如用于开源竞品请先联系群主沟通，未经审核视为侵权

请不要删除和修改 并元国产开发平台 源码头部的版权与作者声明及出处