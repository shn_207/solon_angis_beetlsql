package cn.angis.db.mapper;

import org.beetl.sql.core.SQLBatchReady;
import org.beetl.sql.core.page.PageRequest;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.InheritMapper;
import org.beetl.sql.mapper.annotation.Root;

import java.util.List;

/**
 * 包名称：cn.angis.db.model
 * 类名称：BaseService
 * 类描述：映射基类
 * 创建人：@author angis.cn
 * 创建日期： 2022/11/28 10:30
 */
public interface AngisMapper<T> extends BaseMapper<T> {

    @InheritMapper
    PageResult<T> selectPage(PageRequest pageRequest, @Root T entity);

    @InheritMapper
    int[] deleteBatch(SQLBatchReady batch);

    @InheritMapper
    List<T> listAll(T entity);

}
