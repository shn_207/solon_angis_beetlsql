package cn.angis.db.fn;

import org.beetl.core.Context;
import org.beetl.core.Function;
import org.beetl.core.misc.PrimitiveArrayUtil;

import java.util.Collection;
import java.util.Map;

/**
 * 判断全局变量是否为“空”，下列情况属于为空·的情况，返回true
 * <ul>
 *
 * <li>变量不存在</li>
 * <li>变量存在，但为null</li>
 * <li>变量存在，但是字符，其长途为0</li>
 * <li>变量存在，但是空集合</li>
 * <li>变量存在，但是空数组</li>
 * </ul>
 * 参数可以一个到多个,如<p>
 * ${isEmpty(list)}
 *
 * @author xiandafu
 */
public class NotEmptyFunction implements Function {

    public Boolean call(Object[] paras, Context ctx) {

        if (paras.length == 0)
            return false;

        if (paras.length == 1) {
            Object result = paras[0];
            return !isEmpty(result);
        }

        for (Object result : paras) {

            boolean isEmpty = isEmpty(result);
            if (!isEmpty) {
                return true;
            }
        }
        return true;

    }

    protected boolean isEmpty(Object result) {
        if (result == null) {
            return true;
        }
        if (result instanceof String) {
            return ((String) result).length() == 0;
        } else if (result instanceof Collection) {
            return ((Collection) result).size() == 0;
        } else if (result instanceof Map) {
            return ((Map) result).size() == 0;
        } else if (result.getClass().isArray()) {
            return result.getClass().getComponentType().isPrimitive()
                    ? PrimitiveArrayUtil.getSize(result) == 0
                    : ((Object[]) result).length == 0;
        } else {
            return false;
        }

    }

}