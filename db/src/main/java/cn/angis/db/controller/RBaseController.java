package cn.angis.db.controller;

import cn.angis.common.api.IErrorCode;
import cn.angis.common.api.ApiErrorCode;
import cn.angis.common.model.R;
import cn.dev33.satoken.stp.StpUtil;

/**
 * 包名称：cn.angis.db.model
 * 类名称：BaseService
 * 类描述：公共controller
 * 创建人：@author angis.cn
 * 创建日期： 2022/11/28 11:38
 */
public class RBaseController {

    /**
     * 获取用户id
     */
    protected String getUserId() {
        return StpUtil.getLoginId("");
    }

    protected <T> R<T> success() {
        return R.restResult(ApiErrorCode.SUCCESS, null);
    }

    protected <T> R<T> success(T obj) {
        return R.success(obj);
    }

    protected <T> R<T> error(String msg) {
        return R.error(msg);
    }

    protected <T> R<T> error(IErrorCode errorCode, T obj) {
        return R.error(errorCode);
    }

    protected <T> R<T> restResult(IErrorCode errorCode, T data) {
        return R.restResult(errorCode, data);
    }

}
