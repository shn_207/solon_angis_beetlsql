package cn.angis.system.mapper;

import cn.angis.db.mapper.AngisMapper;
import cn.angis.system.model.Dept;
import org.beetl.sql.mapper.annotation.SqlResource;

/**
* 包名称：cn.angis.system.mapper
* 类名称：DeptMapper
* 类描述：
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@SqlResource("dept")
public interface DeptMapper extends AngisMapper<Dept> {

}