package cn.angis.system.dto.output;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 登录使用
 * @Author：chow
 * @Date: 2023-01-05
 */
@Getter
@Setter
public class LoginOutput implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户id
     */
    private String id;

    /**
     * 部门id
     */
    private String deptId;
    /**
     *用户名
     */
    private String username;
    /**
     *用户昵称
     */
    private String nickName;
    /**
     *用户类型（0、管理员；1、普通用户）
     */
    private String userType;
    /**
     *用户邮箱
     */
    private String email;
    /**
     *手机号码
     */
    private String phoneNumber;
    /**
     *性别（0、男；1、女）
     */
    private String sex;
    /**
     *头像
     */
    private String avatarPath;
    /**
     *帐号状态（0、正常；1、禁用）
     */
    private String userStatus;
    /**
     *资源信息
     */
    private List<String> resources;
    /**
     *部门名称
     */
    private String deptName;
    /**
     *角色名称
     */
    private String roleName;

    public String token;
}
