package cn.angis.system.dto.output;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.output
* 类名称：RoleresOutput
* 类描述：角色和资源关联表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class RoleresOutput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 角色id
	 */
	private String roleId;

	/**
	 * 资源id
	 */
	private String resId;

}