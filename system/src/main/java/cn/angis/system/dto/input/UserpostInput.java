package cn.angis.system.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.input
* 类名称：UserpostInput
* 类描述：用户与岗位关联表
* 创建人：@author angis.cn
* 创建日期： 2023-01-06
*/
@Data
public class UserpostInput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 岗位id
	 */
	private String postId;

}