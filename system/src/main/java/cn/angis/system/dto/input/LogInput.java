package cn.angis.system.dto.input;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* 包名称：cn.angis.system.dto.input
* 类名称：LogInput
* 类描述：日志表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class LogInput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 主键id
	 */
	private String id;

	/**
	 * 操作人员
	 */
	private String createBy;

	/**
	 * 请求参数
	 */
	private String operParam;

	/**
	 * 请求地址
	 */
	private String url;

	/**
	 * ip地址
	 */
	private String ip;

	/**
	 * 业务模块名称
	 */
	private String businessName;

	/**
	 * 方法名
	 */
	private String method;

	/**
	 * 返回结果
	 */
	private String result;

	/**
	 * 操作状态（0正常 1异常）
	 */
	private String logStatus;

	/**
	 * 错误信息
	 */
	private String error;

	private Date createDate;

}