package cn.angis.system.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.input
* 类名称：DeptInput
* 类描述：部门表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class DeptInput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 部门id
	 */
	private String id;

	/**
	 * 父部门id
	 */
	private Long parentId;

	/**
	 * 部门名称
	 */
	private String name;

	/**
	 * 显示顺序
	 */
	private Long sort;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 部门状态（0、正常；1、停用）
	 */
	private String status;

}