package cn.angis.system.dto.input;

import lombok.Getter;
import lombok.Setter;
import org.noear.solon.validation.annotation.NotBlank;

import java.io.Serializable;

/**
 * @Author：chow
 * @Description: 登录使用
 * @Date: 2020/12/17 7:56
 */
@Getter
@Setter
public class LoginInput implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

}
