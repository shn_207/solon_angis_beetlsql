package cn.angis.system.dto.output;

import cn.angis.system.model.Resource;
import lombok.Data;
import org.beetl.sql.annotation.entity.Column;

import java.io.Serializable;
import java.util.List;

/**
* 包名称：cn.angis.system.dto.output
* 类名称：ResourceOutput
* 类描述：资源表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class ResourceOutput implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 主键id
	 */

	private String id;

	/**
	 * 父级id
	 */

	private String parentId;

	/**
	 * 标题（目录名称、菜单名称、按钮名称）
	 */

	private String title;

	/**
	 * 类型（1、目录；2、菜单；3、按钮）
	 */

	private String type;

	/**
	 * 权限标识（菜单和按钮）
	 */

	private String permission;

	/**
	 * 前台组件路径
	 */

	private String vuePath;

	private String componentName;

	/**
	 * 请求方式（GET或者POST等等）
	 */

	private String httpMethod;

	/**
	 * 路由地址（后台url）
	 */

	private String routePath;

	/**
	 * 状态（0、正常；1、禁用）
	 */

	private String resStatus;

	/**
	 * 排序
	 */

	private Long resSort;

	/**
	 * 外链菜单（1：是；0：否）
	 */

	private String menuExtFlag;

	/**
	 * 菜单缓存（1：是；0：否）
	 */

	private String menuCacheFlag;

	/**
	 * 菜单和目录可见（1：是；0：否）
	 */

	private String menuHiddenFlag;

	/**
	 * 菜单图标
	 */

	private String menuIcon;

	private List<Resource> children;

}