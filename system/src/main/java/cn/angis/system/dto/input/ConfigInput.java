package cn.angis.system.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.input
* 类名称：ConfigInput
* 类描述：参数配置表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class ConfigInput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 参数id
	 */
	private String id;

	/**
	 * 参数名称
	 */
	private String name;

	/**
	 * 参数键名
	 */
	private String key;

	/**
	 * 参数键值
	 */
	private String value;

	/**
	 * 系统内置
	 */
	private String type;

}