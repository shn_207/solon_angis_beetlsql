package cn.angis.system.dto.output;

import cn.angis.system.model.Dept;
import lombok.Data;
import org.beetl.sql.annotation.entity.Column;

import java.io.Serializable;
import java.util.List;

/**
* 包名称：cn.angis.system.dto.output
* 类名称：DeptOutput
* 类描述：部门表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class DeptOutput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 部门id
	 */
	private String id;

	/**
	 * 父部门id
	 */
	private String parentId;

	/**
	 * 祖级列表
	 */
	private String parentIds;

	/**
	 * 部门名称
	 */
	private String name;

	/**
	 * 显示顺序
	 */
	private int sort;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 部门状态（0、正常；1、停用）
	 */
	private String status;

	private List<DeptOutput> children;

}