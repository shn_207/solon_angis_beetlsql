package cn.angis.system.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.system.dto.input.LogInput;
import cn.angis.system.dto.output.LogOutput;
import cn.angis.system.dtomapper.LogStruct;
import cn.angis.system.model.Log;
import cn.angis.system.service.LogService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 日志表前端控制器
* @author angis.cn
* @Date 2023-01-05
*/
@SLog("日志管理")
@Mapping("/system/log/")
@Controller
public class LogController extends BaseController<LogService, Log> {

    /**
    * 分页查询
    * @param logInput
    * @param pageModel
    * @return R<PageResult<LogOutput>>
    * @Date 2023-01-05
    */
    @Get
    @SLog
    @Mapping("page")
    public R<PageResult<LogOutput>> page(LogInput logInput, PageModel pageModel) {
        Log log = LogStruct.INSTANCE.toLog(logInput);
        PageResult<Log> pageResult = pageListByEntity(log, pageModel);
        List<LogOutput> logOutputList = LogStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, logOutputList));
    }

    /**
    * 保存
    * @param logInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @Mapping("save")
    public R<Boolean> save(@Validated LogInput logInput) {
    Log log = LogStruct.INSTANCE.toLog(logInput);
        return super.save(log);
    }

    /**
    * 修改
    * @param logInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @Mapping("update")
    public R<Boolean> update(@Validated LogInput logInput) {
        Log log = LogStruct.INSTANCE.toLog(logInput);
        return super.update(log);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Log.class, ids);
    }
}
