package cn.angis.system.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.system.dto.input.PostInput;
import cn.angis.system.dto.output.PostOutput;
import cn.angis.system.dtomapper.PostStruct;
import cn.angis.system.model.Post;
import cn.angis.system.service.PostService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 岗位表前端控制器
* @author angis.cn
* @Date 2023-01-05
*/
@SLog("岗位管理")
@Mapping("/system/post/")
@Controller
public class PostController extends BaseController<PostService, Post> {

    /**
    * 分页查询
    * @param postInput
    * @param pageModel
    * @return R<PageResult<PostOutput>>
    * @Date 2023-01-05
    */
    @Get
    @SLog
    @Mapping("page")
    public R<PageResult<PostOutput>> page(PostInput postInput, PageModel pageModel) {
        Post post = PostStruct.INSTANCE.toPost(postInput);
        PageResult<Post> pageResult = pageListByEntity(post, pageModel);
        List<PostOutput> postOutputList = PostStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, postOutputList));
    }

    /**
    * 保存
    * @param postInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @org.noear.solon.annotation.Post
    @SLog
    @Mapping("save")
    public R<Boolean> save(@Validated PostInput postInput) {
    Post post = PostStruct.INSTANCE.toPost(postInput);
        return super.save(post);
    }

    /**
    * 修改
    * @param postInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @org.noear.solon.annotation.Post
    @SLog
    @Mapping("update")
    public R<Boolean> update(@Validated PostInput postInput) {
        Post post = PostStruct.INSTANCE.toPost(postInput);
        return super.update(post);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @org.noear.solon.annotation.Post
    @SLog
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @org.noear.solon.annotation.Post
    @SLog
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Post.class, ids);
    }

    @Get
    @SLog
    @Mapping("all")
    public R<List<PostOutput>> listAll() {
        return success(PostStruct.INSTANCE.toOutputList(all(new Post())));
    }
}
