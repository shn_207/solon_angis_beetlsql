package cn.angis.system.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.system.dto.input.RoleInput;
import cn.angis.system.dto.output.RoleOutput;
import cn.angis.system.dtomapper.RoleStruct;
import cn.angis.system.model.Role;
import cn.angis.system.service.RoleService;
import cn.angis.system.service.RoledeptService;
import cn.angis.system.service.RoleresService;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 角色表前端控制器
* @author angis.cn
* @Date 2023-01-05
*/
@SLog("角色管理")
@Mapping("/system/role/")
@Controller
public class RoleController extends BaseController<RoleService, Role> {

    @Inject
    private RoledeptService roledeptService;
    @Inject
    private RoleresService roleresService;
    /**
    * 分页查询
    * @param roleInput
    * @param pageModel
    * @return R<PageResult<RoleOutput>>
    * @Date 2023-01-05
    */
    @Get
    @SLog
    @Mapping("page")
    public R<PageResult<RoleOutput>> page(RoleInput roleInput, PageModel pageModel) {
        Role role = RoleStruct.INSTANCE.toRole(roleInput);
        PageResult<Role> pageResult = pageListByEntity(role, pageModel);
        List<RoleOutput> roleOutputList = RoleStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, roleOutputList));
    }

    /**
    * 保存
    * @param roleInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("save")
    public R<Boolean> save(@Validated RoleInput roleInput) {
        Role role = RoleStruct.INSTANCE.toRole(roleInput);
        R<Boolean> result = super.save(role);
        roledeptService.saveRoleDepts(role.getId(), roleInput.getDeptIds());
        roleresService.saveRoleRes(roleInput.getUserId(), role.getId(), roleInput.getResIds());
        return result;
    }

    /**
    * 修改
    * @param roleInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("update")
    public R<Boolean> update(@Validated RoleInput roleInput) {
        Role role = RoleStruct.INSTANCE.toRole(roleInput);
        roledeptService.saveRoleDepts(roleInput.getId(), roleInput.getDeptIds());
        roleresService.saveRoleRes(roleInput.getUserId(), roleInput.getId(), roleInput.getResIds());
        return super.update(role);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Role.class, ids);
    }

    @Get
    @Mapping("getDataScopeDeptIdsByRoleId")
    public R<List<String>> getDataScopeDeptIdsByRoleId(@NotBlank(message = "角色id不能为空") String roleId) {
        return success(baseService.getDataScopeDeptIdsByRoleId(roleId));
    }

    @Get
    @SLog
    @Mapping("all")
    public R<List<RoleOutput>> listAll() {
        return success(RoleStruct.INSTANCE.toOutputList(all(new Role())));
    }
}
