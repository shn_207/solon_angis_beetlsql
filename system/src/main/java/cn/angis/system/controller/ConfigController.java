package cn.angis.system.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.system.dto.input.ConfigInput;
import cn.angis.system.dto.output.ConfigOutput;
import cn.angis.system.dtomapper.ConfigStruct;
import cn.angis.system.model.Config;
import cn.angis.system.service.ConfigService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 参数配置表前端控制器
* @author angis.cn
* @Date 2023-01-05
*/
@SLog("参数配置")
@Mapping("/system/config/")
@Controller
public class ConfigController extends BaseController<ConfigService, Config> {

    /**
    * 分页查询
    * @param configInput
    * @param pageModel
    * @return R<PageResult<ConfigOutput>>
    * @Date 2023-01-05
    */
    @SLog
    @Get
    @Mapping("page")
    public R<PageResult<ConfigOutput>> page(ConfigInput configInput, PageModel pageModel) {
        String str = "";
        Config config = ConfigStruct.INSTANCE.toConfig(configInput);
        PageResult<Config> pageResult = pageListByEntity(config, pageModel);
        List<ConfigOutput> configOutputList = ConfigStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, configOutputList));
    }

    /**
    * 保存
    * @param configInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @SLog
    @Post
    @Mapping("save")
    public R<Boolean> save(@Validated ConfigInput configInput) {
    Config config = ConfigStruct.INSTANCE.toConfig(configInput);
        return super.save(config);
    }

    /**
    * 修改
    * @param configInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @SLog
    @Post
    @Mapping("update")
    public R<Boolean> update(@Validated ConfigInput configInput) {
        Config config = ConfigStruct.INSTANCE.toConfig(configInput);
        return super.update(config);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @SLog
    @Post
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @SLog
    @Post
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Config.class, ids);
    }
}
