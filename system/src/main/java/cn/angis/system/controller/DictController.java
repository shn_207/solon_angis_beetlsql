package cn.angis.system.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.constant.Constant;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.system.dto.input.DictInput;
import cn.angis.system.dto.output.DictOutput;
import cn.angis.system.dtomapper.DictStruct;
import cn.angis.system.model.Dict;
import cn.angis.system.service.DictService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.data.annotation.CacheRemove;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;
import java.util.Map;

/**
* 字典数据表前端控制器
* @author angis.cn
* @Date 2023-01-05
*/
@SLog("字典管理")
@Mapping("/system/dict/")
@Controller
public class DictController extends BaseController<DictService, Dict> {

    /**
    * 分页查询
    * @param dictInput
    * @param pageModel
    * @return R<PageResult<DictOutput>>
    * @Date 2023-01-05
    */
    @Get
    @SLog
    @Mapping("page")
    public R<PageResult<DictOutput>> page(DictInput dictInput, PageModel pageModel) {
        Dict dict = DictStruct.INSTANCE.toDict(dictInput);
        PageResult<Dict> pageResult = pageListByEntity(dict, pageModel);
        List<DictOutput> dictOutputList = DictStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, dictOutputList));
    }

    /**
     * listByType
     * @param types
     * @return: R<List<DictOutput>>
     * @throws: 
     * @Date: 2023/1/18     
     */
    @Get
    @Mapping("mapByType")
    public R<Map<String, List<DictOutput>>> mapByType(@NotBlank String types) {
        return success(baseService.getMapByTypes(types));
    }

    /**
    * 保存
    * @param dictInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @CacheRemove(tags = Constant.CACHE_DICT_MAP)
    @Post
    @SLog
    @Mapping("save")
    public R<Boolean> save(@Validated DictInput dictInput) {
    Dict dict = DictStruct.INSTANCE.toDict(dictInput);
        return super.save(dict);
    }

    /**
    * 修改
    * @param dictInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @CacheRemove(tags = Constant.CACHE_DICT_MAP)
    @Post
    @SLog
    @Mapping("update")
    public R<Boolean> update(@Validated DictInput dictInput) {
        Dict dict = DictStruct.INSTANCE.toDict(dictInput);
        return super.update(dict);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @CacheRemove(tags = Constant.CACHE_DICT_MAP)
    @Post
    @SLog
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @CacheRemove(tags = Constant.CACHE_DICT_MAP)
    @Post
    @SLog
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Dict.class, ids);
    }
}
