package cn.angis.system.service;

import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.UserpostMapper;
import cn.angis.system.model.Userpost;
import cn.hutool.core.collection.CollUtil;
import org.beetl.sql.core.SQLReady;
import org.noear.solon.annotation.ProxyComponent;
import org.noear.solon.data.annotation.Tran;

import java.util.ArrayList;
import java.util.List;

/**
* 用户与岗位关联表服务
* @author angis.cn
* @Date 2023-01-06
*/
@ProxyComponent
public class UserpostService extends BaseService<UserpostMapper, Userpost> {
    @Tran
    public int deleteByUserId(String userId) {
        return baseMapper.getSQLManager().executeUpdate(new SQLReady("delete from sys_user_post where user_id=?", userId));
    }

    /**
     * 保存用户岗位关联表
     * @param userId
 * @param postIds
     * @return: void
     * @throws: 
     * @Date: 2023/1/8     
     */
    @Tran
    public void saveUserPost(String userId, List<String> postIds) {
        if (CollUtil.isNotEmpty(postIds)) {
            this.deleteByUserId(userId);
            List<Userpost> userpostList = new ArrayList<>();
            postIds.forEach(postId -> userpostList.add(new Userpost(userId, postId)));
            this.saveBatch(userpostList);
        }
    }
}