package cn.angis.system.service;

import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.UserroleMapper;
import cn.angis.system.model.Userrole;
import cn.hutool.core.collection.CollUtil;
import org.beetl.sql.core.SQLReady;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.ProxyComponent;
import org.noear.solon.data.annotation.Tran;

import java.util.ArrayList;
import java.util.List;

/**
* 用户角色关联表服务
* @author angis.cn
* @Date 2023-01-06
*/
@ProxyComponent
public class UserroleService extends BaseService<UserroleMapper, Userrole> {
    @Inject
    private RoleresService roleresService;

    @Inject
    private UserService userService;

    @Tran
    public int deleteByUserId(String userId) {
        return baseMapper.getSQLManager().executeUpdate(new SQLReady("delete from sys_user_role where user_id=?", userId));
    }

    /**
     * 可在用户角色关联表
     * @param userId
 * @param roleIds
     * @return: void
     * @throws: 
     * @Date: 2023/1/8     
     */
    @Tran
    public void saveUserRole(String userId, List<String> roleIds) {
        if (CollUtil.isNotEmpty(roleIds)) {
            this.deleteByUserId(userId);
            List<Userrole> userroleList = new ArrayList<>();
            roleIds.forEach(roleId -> userroleList.add(new Userrole(userId, roleId)));
            if (userroleList.size()>0) {
                this.saveBatch(userroleList);
                userService.refreshUserByUserId(userId);
            }
        }
    }


}