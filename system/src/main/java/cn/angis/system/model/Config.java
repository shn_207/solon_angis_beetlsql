package cn.angis.system.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

/**
* 包名称：cn.angis.system.model
* 类名称：Config
* 类描述：参数配置表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Table(name="sys_config")
@Data
public class Config extends BaseModel {

	/**
	 * 参数id
	 */
	private String id;

	/**
	 * 参数名称
	 */
	private String name;

	/**
	 * 参数键名
	 */
	private String key;

	/**
	 * 参数键值
	 */
	private String value;

	/**
	 * 系统内置
	 */
	private String type;

}