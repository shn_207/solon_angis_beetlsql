package cn.angis.system.model;

import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.model
* 类名称：Userpost
* 类描述：用户与岗位关联表
* 创建人：@author angis.cn
* 创建日期： 2023-01-06
*/
@Table(name="sys_user_post")
@Data
public class Userpost implements Serializable {
	private static final long sserialVersionUID = 1L;
	public Userpost() {}
	public Userpost(String userId, String postId) {
		this.postId = postId;
		this.userId = userId;
	}
	/**
	 * 用户id
	 */

	@Column("user_id")
	private String userId;

	/**
	 * 岗位id
	 */

	@Column("post_id")
	private String postId;

}