package cn.angis.system.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.LogicDelete;
import org.beetl.sql.annotation.entity.Table;

/**
* 包名称：cn.angis.system.model
* 类名称：Role
* 类描述：角色表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Table(name="sys_role")
@Data
public class Role extends BaseModel {

	/**
	 * 角色id
	 */

	private String id;

	/**
	 * 角色名称
	 */

	private String name;

	/**
	 * 角色权限字符串
	 */

	private String code;

	/**
	 * 角色状态（0、正常；1、禁用）
	 */

	private String status;

	/**
	 * 数据范围（1、全部数据权限；2、自定数据权限；3、本部门数据权限；4、本部门及以下数据权限）
	 */

	@Column("data_scope")
	private String dataScope;

	/**
	 * 删除标识（0：正常；1：已删除）
	 */
	@LogicDelete(1)
	@Column("del_flag")
	private Integer delFlag;

}