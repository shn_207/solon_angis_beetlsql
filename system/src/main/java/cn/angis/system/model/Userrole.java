package cn.angis.system.model;

import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.model
* 类名称：Userrole
* 类描述：用户角色关联表
* 创建人：@author angis.cn
* 创建日期： 2023-01-06
*/
@Table(name="sys_user_role")
@Data
public class Userrole implements Serializable {
	private static final long sserialVersionUID = 1L;
	public Userrole(){}
	public Userrole(String userId, String roleId) {
		this.roleId = roleId;
		this.userId = userId;
	}
	/**
	 * 用户id
	 */

	@Column("user_id")
	private String userId;

	/**
	 * 角色id
	 */

	@Column("role_id")
	private String roleId;

}