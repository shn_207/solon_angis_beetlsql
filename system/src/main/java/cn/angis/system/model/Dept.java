package cn.angis.system.model;

import cn.angis.common.model.TreeModel;
import lombok.Data;
import lombok.experimental.Accessors;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.LogicDelete;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;
import java.util.List;

/**
* 包名称：cn.angis.system.model
* 类名称：Dept
* 类描述：部门表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Table(name="sys_dept")
@Data
@Accessors(chain = true)
public class Dept implements TreeModel<Dept>, Serializable {

	/**
	 * 部门id
	 */

	private String id;

	/**
	 * 父部门id
	 */

	@Column("parent_id")
	private String parentId;

	/**
	 * 祖级列表
	 */

	@Column("parent_ids")
	private String parentIds;

	/**
	 * 部门名称
	 */

	private String name;

	/**
	 * 显示顺序
	 */

	private int sort;

	/**
	 * 联系电话
	 */

	private String phone;

	/**
	 * 邮箱
	 */

	private String email;

	/**
	 * 部门状态（0、正常；1、停用）
	 */

	private String status;

	/**
	 * 删除标识（0：正常；1：已删除）
	 */
	@LogicDelete(1)
	@Column("del_flag")
	private Integer delFlag;

	private List<Dept> children;
}