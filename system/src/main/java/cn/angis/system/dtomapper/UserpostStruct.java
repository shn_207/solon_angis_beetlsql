package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.UserpostInput;
import cn.angis.system.dto.output.UserpostOutput;
import cn.angis.system.model.Userpost;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 用户与岗位关联表转换类
* @author angis.cn
* @Date 2023-01-06
*/
@Mapper
public interface UserpostStruct {
    UserpostStruct INSTANCE = Mappers.getMapper(UserpostStruct.class);
    Userpost toUserpost(UserpostInput userpostInput);
    UserpostOutput toOutput(Userpost userpost);
    List<UserpostOutput> toOutputList(List<Userpost> userpostList);
}
