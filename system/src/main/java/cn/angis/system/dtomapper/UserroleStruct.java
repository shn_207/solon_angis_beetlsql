package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.UserroleInput;
import cn.angis.system.dto.output.UserroleOutput;
import cn.angis.system.model.Userrole;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 用户角色关联表转换类
* @author angis.cn
* @Date 2023-01-06
*/
@Mapper
public interface UserroleStruct {
    UserroleStruct INSTANCE = Mappers.getMapper(UserroleStruct.class);
    Userrole toUserrole(UserroleInput userroleInput);
    UserroleOutput toOutput(Userrole userrole);
    List<UserroleOutput> toOutputList(List<Userrole> userroleList);
}
