package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.RoledeptInput;
import cn.angis.system.dto.output.RoledeptOutput;
import cn.angis.system.model.Roledept;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 转换类
* @author angis.cn
* @Date 2023-01-06
*/
@Mapper
public interface RoledeptStruct {
    RoledeptStruct INSTANCE = Mappers.getMapper(RoledeptStruct.class);
    Roledept toRoledept(RoledeptInput roledeptInput);
    RoledeptOutput toOutput(Roledept roledept);
    List<RoledeptOutput> toOutputList(List<Roledept> roledeptList);
}
