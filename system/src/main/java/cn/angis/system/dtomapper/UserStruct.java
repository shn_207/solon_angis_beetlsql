package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.UserInput;
import cn.angis.system.dto.output.LoginOutput;
import cn.angis.system.dto.output.UserOutput;
import cn.angis.system.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 用户表转换类
* @author angis.cn
* @Date 2023-02-21
*/
@Mapper
public interface UserStruct {
    UserStruct INSTANCE = Mappers.getMapper(UserStruct.class);
    User toUser(UserInput userInput);
    UserOutput toOutput(User user);
    List<UserOutput> toOutputList(List<User> userList);
    LoginOutput toLoginOutPut(User user);
}
