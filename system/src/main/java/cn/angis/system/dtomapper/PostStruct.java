package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.PostInput;
import cn.angis.system.dto.output.PostOutput;
import cn.angis.system.model.Post;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 岗位表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface PostStruct {
    PostStruct INSTANCE = Mappers.getMapper(PostStruct.class);
    Post toPost(PostInput postInput);
    PostOutput toOutput(Post post);
    List<PostOutput> toOutputList(List<Post> postList);
}
