package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.RoleInput;
import cn.angis.system.dto.output.RoleOutput;
import cn.angis.system.model.Role;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 角色表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface RoleStruct {
    RoleStruct INSTANCE = Mappers.getMapper(RoleStruct.class);
    Role toRole(RoleInput roleInput);
    RoleOutput toOutput(Role role);
    List<RoleOutput> toOutputList(List<Role> roleList);
}
