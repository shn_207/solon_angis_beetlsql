package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.ResourceInput;
import cn.angis.system.dto.output.ResourceOutput;
import cn.angis.system.model.Resource;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 资源表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface ResourceStruct {
    ResourceStruct INSTANCE = Mappers.getMapper(ResourceStruct.class);
    Resource toResource(ResourceInput resourceInput);
    ResourceOutput toOutput(Resource resource);
    List<ResourceOutput> toOutputList(List<Resource> resourceList);
}
