cols
===
```sql
id,parent_id,label,value,type,sort,type_cn,status
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from sys_dict where 1=1
-- @if(isNotEmpty(label)){
and label like #{label}
-- @}
```