cols
===
```sql
id,name,code,status,data_scope
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from sys_role where 1=1
-- @if(isNotEmpty(name)){
and name like #{name}
-- @}
```

selectRoleKeyByUserId
===
```sql
SELECT distinct role.code
FROM sys_user user 
                        INNER JOIN sys_user_role ur on user.id = ur.user_id
    INNER JOIN sys_role role on ur.role_id = role.id
WHERE user.del_flag = '0'
-- @if(isNotEmpty(userId)){
  and user.id=#{userId}
-- @}
```

getRoleListByUserId
===
```sql
SELECT distinct
    role.id AS id,role.name,role.code,role.status,role.data_scope as dataScope
FROM sys_user user 
                        INNER JOIN sys_user_role ur on user.id = ur.user_id
    INNER JOIN sys_role role on ur.role_id = role.id
WHERE user.del_flag = '0'
-- @if(isNotEmpty(userId)){
  and user.id=#{userId}
-- @}
```