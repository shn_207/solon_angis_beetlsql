cols
===
```sql
id,oper_name AS createBy,oper_param as operParam,url,ip,business_name as businessName,method,result,log_status as logStatus,error,create_date as createDate
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from sys_log where 1=1
-- @if(isNotEmpty(createBy)){
and oper_name like #{createBy}
-- @}
```