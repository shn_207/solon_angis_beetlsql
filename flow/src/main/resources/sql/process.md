cols
===
```sql
id,name,display_Name AS displayName,type,instance_Url AS instanceUrl,state,content,version,create_by AS createBy,create_date AS createDate,update_by AS updateBy,update_date AS updateDate,del_flag AS delFlag
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from flow_process where 1=1
-- @if(isNotEmpty(id)){
and id=#{id}
-- @}
-- @if(isNotEmpty(name)){
and name=#{name}
-- @}
-- @if(isNotEmpty(displayName)){
and display_Name=#{displayName}
-- @}
-- @if(isNotEmpty(type)){
and type=#{type}
-- @}
-- @if(isNotEmpty(instanceUrl)){
and instance_Url=#{instanceUrl}
-- @}
-- @if(isNotEmpty(state)){
and state=#{state}
-- @}
-- @if(isNotEmpty(content)){
and content=#{content}
-- @}
-- @if(isNotEmpty(version)){
and version=#{version}
-- @}
-- @if(isNotEmpty(createBy)){
and create_by=#{createBy}
-- @}
-- @if(isNotEmpty(createDate)){
and create_date=#{createDate}
-- @}
-- @if(isNotEmpty(updateBy)){
and update_by=#{updateBy}
-- @}
-- @if(isNotEmpty(updateDate)){
and update_date=#{updateDate}
-- @}
-- @if(isNotEmpty(delFlag)){
and del_flag=#{delFlag}
-- @}
```