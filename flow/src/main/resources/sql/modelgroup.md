cols
===
```sql
id,name,remark,create_by AS createBy,create_date AS createDate,update_by AS updateBy,update_date AS updateDate
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from flow_model_group where 1=1
-- @if(isNotEmpty(id)){
and id=#{id}
-- @}
-- @if(isNotEmpty(name)){
and name=#{name}
-- @}
-- @if(isNotEmpty(remark)){
and remark=#{remark}
-- @}
-- @if(isNotEmpty(createBy)){
and create_by=#{createBy}
-- @}
-- @if(isNotEmpty(createDate)){
and create_date=#{createDate}
-- @}
-- @if(isNotEmpty(updateBy)){
and update_by=#{updateBy}
-- @}
-- @if(isNotEmpty(updateDate)){
and update_date=#{updateDate}
-- @}
```