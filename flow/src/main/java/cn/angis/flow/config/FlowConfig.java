package cn.angis.flow.config;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import org.snaker.engine.DBAccess;
import org.snaker.engine.SnakerEngine;

import javax.sql.DataSource;

/**
 * 包名称：cn.angis.flow.config
 * 类名称：FlowConfig
 * 类描述：
 * 创建人：@author angis.cn
 * 创建日期： 2023/5/11 12:08
 */
@Configuration
public class FlowConfig {

    @Inject
    private DataSource dataSource;

    @Bean
    public SnakerEngine getEngine() {
        return new org.snaker.engine.cfg.Configuration()
                .initAccessDBObject(dataSource)
                .buildSnakerEngine();
    }
}
