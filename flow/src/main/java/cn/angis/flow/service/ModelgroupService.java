package cn.angis.flow.service;

import cn.angis.db.service.BaseService;
import cn.angis.flow.mapper.ModelgroupMapper;
import cn.angis.flow.model.Modelgroup;
import org.noear.solon.aspect.annotation.Service;

/**
* 模型分组服务
* @author angis.cn
* @Date 2023-05-04
*/
@Service
public class ModelgroupService extends BaseService<ModelgroupMapper, Modelgroup> {
}