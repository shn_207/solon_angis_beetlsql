package cn.angis.flow.service;

import cn.angis.db.service.BaseService;
import cn.angis.flow.mapper.ProcessMapper;
import cn.angis.flow.model.Process;
import org.noear.solon.aspect.annotation.Service;

/**
* 流程定义表服务
* @author angis.cn
* @Date 2023-05-04
*/
@Service
public class ProcessService extends BaseService<ProcessMapper, Process> {
}