package cn.angis.flow.access;

import org.noear.solon.data.tran.TranUtils;
import org.snaker.engine.access.transaction.DataSourceTransactionInterceptor;
import org.snaker.engine.access.transaction.TransactionStatus;

/**
 * 包名称：cn.angis.flow.access
 * 类名称：SnakerTransactionInterceptor
 * 类描述：
 * 创建人：@author angis.cn
 * 创建日期： 2023/5/11 14:03
 */
public class SnakerTransactionInterceptor extends DataSourceTransactionInterceptor {


    public void initialize(Object accessObject) {
        //ignore
    }

    protected TransactionStatus getTransaction() {
        return new TransactionStatus(null, TranUtils.inTrans());
    }

    protected void commit(TransactionStatus status) {
        if (TranUtils.inTrans() == false) {
            super.commit(status);
        }

    }

    protected void rollback(TransactionStatus status) {
        if (TranUtils.inTrans() == false) {
            super.rollback(status);
        }
    }
}
