package cn.angis.flow.access;

import com.zaxxer.hikari.HikariDataSource;
import org.noear.solon.Solon;
import org.snaker.engine.access.jdbc.JdbcAccess;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 包名称：cn.angis.flow.access
 * 类名称：SnakerAccess
 * 类描述：
 * 创建人：@author angis.cn
 * 创建日期： 2023/5/11 13:55
 */
public class SnakerAccess extends JdbcAccess {
    protected Connection getConnection() throws SQLException {
        HikariDataSource dataSource = Solon.context().getBean(HikariDataSource.class);
        Connection conn = dataSource.getConnection();
        return conn;
    }
}
