package cn.angis.flow.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.flow.dto.input
* 类名称：ModelgroupInput
* 类描述：模型分组
* 创建人：@author angis.cn
* 创建日期： 2023-05-04
*/
@Data
public class ModelgroupInput implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 分组名称
	 */
	private String name;

	/**
	 * 备注
	 */
	private String remark;

}