package cn.angis.flow.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.flow.dto.input
* 类名称：HistorderInput
* 类描述：历史流程实例表
* 创建人：@author angis.cn
* 创建日期： 2023-05-11
*/
@Data
public class HistorderInput implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 主键ID
	 */
	private String id;

	/**
	 * 流程定义ID
	 */
	private String processId;

	/**
	 * 状态
	 */
	private Integer orderState;

	/**
	 * 发起人
	 */
	private String creator;

	/**
	 * 发起时间
	 */
	private String createTime;

	/**
	 * 完成时间
	 */
	private String endTime;

	/**
	 * 期望完成时间
	 */
	private String expireTime;

	/**
	 * 优先级
	 */
	private Integer priority;

	/**
	 * 父流程ID
	 */
	private String parentId;

	/**
	 * 流程实例编号
	 */
	private String orderNo;

	/**
	 * 附属变量json存储
	 */
	private String variable;

	private String displayName;

	private String code;

}