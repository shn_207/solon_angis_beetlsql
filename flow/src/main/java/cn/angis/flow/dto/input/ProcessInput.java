package cn.angis.flow.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.flow.dto.input
* 类名称：ProcessInput
* 类描述：流程定义表
* 创建人：@author angis.cn
* 创建日期： 2023-05-04
*/
@Data
public class ProcessInput implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 主键ID
	 */
	private Long id;

	/**
	 * 流程名称
	 */
	private String name;

	/**
	 * 流程显示名称
	 */
	private String displayName;

	/**
	 * 流程类型
	 */
	private String type;

	/**
	 * 实例url
	 */
	private String instanceUrl;

	/**
	 * 流程是否可用
	 */
	private Integer state;

	/**
	 * 流程模型定义
	 */
	private byte[] content;

	/**
	 * 版本
	 */
	private Integer version;

	/**
	 * 删除标识
	 */
	private String delFlag;

}