package cn.angis.flow.mapper;

import cn.angis.db.mapper.AngisMapper;
import cn.angis.flow.model.Historder;
import org.beetl.sql.mapper.annotation.SqlResource;

/**
* 包名称：cn.angis.flow.mapper
* 类名称：HistorderMapper
* 类描述：
* 创建人：@author angis.cn
* 创建日期： 2023-05-11
*/
@SqlResource("historder")
public interface HistorderMapper extends AngisMapper<Historder> {

}