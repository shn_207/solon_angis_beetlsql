package cn.angis.flow.mapper;

import cn.angis.db.mapper.AngisMapper;
import cn.angis.flow.model.Process;
import org.beetl.sql.mapper.annotation.SqlResource;

/**
* 包名称：cn.angis.flow.mapper
* 类名称：ProcessMapper
* 类描述：
* 创建人：@author angis.cn
* 创建日期： 2023-05-04
*/
@SqlResource("process")
public interface ProcessMapper extends AngisMapper<Process> {

}