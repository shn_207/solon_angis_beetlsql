package cn.angis.flow.dtomapper;

import cn.angis.flow.dto.input.HistorderInput;
import cn.angis.flow.dto.output.HistorderOutput;
import cn.angis.flow.model.Historder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.snaker.engine.entity.HistoryOrder;

import java.util.List;

/**
* 历史流程实例表转换类
* @author angis.cn
* @Date 2023-05-11
*/
@Mapper
public interface HistoryOrderStruct {
    HistoryOrderStruct INSTANCE = Mappers.getMapper(HistoryOrderStruct.class);
    Historder toHistorder(HistoryOrder historyOrder);
    HistorderOutput toOutput(HistoryOrder historyOrder);
    List<HistorderOutput> toOutputList(List<HistoryOrder> historyOrderList);
    List<Historder> toHistorderList(List<HistoryOrder> historyOrderList);
}
