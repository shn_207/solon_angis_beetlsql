package cn.angis.flow.dtomapper;

import cn.angis.flow.dto.input.ProcessInput;
import cn.angis.flow.dto.output.ProcessOutput;
import cn.angis.flow.model.Process;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 流程定义表转换类
* @author angis.cn
* @Date 2023-05-04
*/
@Mapper
public interface ProcessStruct {
    ProcessStruct INSTANCE = Mappers.getMapper(ProcessStruct.class);
    Process toProcess(ProcessInput processInput);
    ProcessOutput toOutput(Process process);
    List<ProcessOutput> toOutputList(List<Process> processList);
}
