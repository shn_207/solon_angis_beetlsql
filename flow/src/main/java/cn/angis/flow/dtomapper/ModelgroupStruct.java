package cn.angis.flow.dtomapper;

import cn.angis.flow.dto.input.ModelgroupInput;
import cn.angis.flow.dto.output.ModelgroupOutput;
import cn.angis.flow.model.Modelgroup;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 模型分组转换类
* @author angis.cn
* @Date 2023-05-04
*/
@Mapper
public interface ModelgroupStruct {
    ModelgroupStruct INSTANCE = Mappers.getMapper(ModelgroupStruct.class);
    Modelgroup toModelgroup(ModelgroupInput modelgroupInput);
    ModelgroupOutput toOutput(Modelgroup modelgroup);
    List<ModelgroupOutput> toOutputList(List<Modelgroup> modelgroupList);
}
