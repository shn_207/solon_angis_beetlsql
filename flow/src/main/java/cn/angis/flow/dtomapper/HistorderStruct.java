package cn.angis.flow.dtomapper;

import cn.angis.flow.dto.input.HistorderInput;
import cn.angis.flow.dto.output.HistorderOutput;
import cn.angis.flow.model.Historder;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 历史流程实例表转换类
* @author angis.cn
* @Date 2023-05-11
*/
@Mapper
public interface HistorderStruct {
    HistorderStruct INSTANCE = Mappers.getMapper(HistorderStruct.class);
    Historder toHistorder(HistorderInput historderInput);
    HistorderOutput toOutput(Historder historder);
    List<HistorderOutput> toOutputList(List<Historder> historderList);
}
