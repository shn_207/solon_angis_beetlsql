package cn.angis.flow.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

/**
* 包名称：cn.angis.flow.model
* 类名称：Modelgroup
* 类描述：模型分组
* 创建人：@author angis.cn
* 创建日期： 2023-05-04
*/
@Table(name="flow_model_group")
@Data
public class Modelgroup extends BaseModel {

	/**
	 * 主键
	 */

	private Long id;

	/**
	 * 分组名称
	 */

	private String name;

	/**
	 * 备注
	 */

	private String remark;

}