package cn.angis.flow.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.InsertIgnore;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.annotation.entity.UpdateIgnore;
import org.noear.solon.validation.annotation.NotNull;

/**
* 包名称：cn.angis.flow.model
* 类名称：Process
* 类描述：流程定义表
* 创建人：@author angis.cn
* 创建日期： 2023-05-04
*/
@Table(name="flow_process")
@Data
public class Process extends BaseModel {

	/**
	 * 主键ID
	 */

	private Long id;

	/**
	 * 流程名称
	 */

	private String name;

	/**
	 * 流程显示名称
	 */

	@Column("display_Name")
	private String displayName;

	/**
	 * 流程类型
	 */

	private String type;

	/**
	 * 实例url
	 */

	@Column("instance_Url")
	private String instanceUrl;

	/**
	 * 流程是否可用
	 */

	private Integer state;

	/**
	 * 流程模型定义
	 */

	private byte[] content;

	/**
	 * 版本
	 */

	private Integer version;

	/**
	 * 删除标识
	 */

	@Column("del_flag")
	private String delFlag;

}