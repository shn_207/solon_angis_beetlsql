package cn.angis.flow.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.InsertIgnore;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.annotation.entity.UpdateIgnore;
import org.noear.solon.validation.annotation.NotNull;

import java.util.Map;

/**
* 包名称：cn.angis.flow.model
* 类名称：Historder
* 类描述：历史流程实例表
* 创建人：@author angis.cn
* 创建日期： 2023-05-11
*/
@Table(name="wf_hist_order")
@Data
public class Historder extends BaseModel {

	/**
	 * 主键ID
	 */

	private String id;

	/**
	 * 流程定义ID
	 */

	@Column("process_Id")
	private String processId;

	/**
	 * 状态
	 */

	@Column("order_State")
	private Integer orderState;

	/**
	 * 发起人
	 */

	private String creator;

	/**
	 * 发起时间
	 */

	@Column("create_Time")
	private String createTime;

	/**
	 * 完成时间
	 */

	@Column("end_Time")
	private String endTime;

	/**
	 * 期望完成时间
	 */

	@Column("expire_Time")
	private String expireTime;

	/**
	 * 优先级
	 */

	private Integer priority;

	/**
	 * 父流程ID
	 */

	@Column("parent_Id")
	private String parentId;

	/**
	 * 流程实例编号
	 */

	@Column("order_No")
	private String orderNo;

	/**
	 * 附属变量json存储
	 */

	private String variable;

	private String displayName;

	private String code;

	private Map<String,Object> args;

}