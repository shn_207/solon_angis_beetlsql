package cn.angis.flow.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.flow.dto.input.HistorderInput;
import cn.angis.flow.dto.output.HistorderOutput;
import cn.angis.flow.dtomapper.HistorderStruct;
import cn.angis.flow.model.Historder;
import cn.angis.flow.service.HistorderService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 历史流程实例表前端控制器
* @author angis.cn
* @Date 2023-05-11
*/
@SLog("历史流程实例表")
@Mapping("/flow/historder/")
@Controller
public class HistorderController extends BaseController<HistorderService, Historder> {

    /**
    * 分页查询
    * @param historderInput
    * @param pageModel
    * @return R<PageResult<HistorderOutput>>
    * @Date 2023-05-11
    */
    @SLog
    @Get
    @Mapping("page")
    public R<PageResult<HistorderOutput>> page(HistorderInput historderInput, PageModel pageModel) {
        Historder historder = HistorderStruct.INSTANCE.toHistorder(historderInput);
        PageResult<Historder> pageResult = baseService.flowPage(historder, pageModel);
        List<HistorderOutput> historderOutputList = HistorderStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, historderOutputList));
    }

    /**
    * 保存
    * @param historderInput
    * @return R<Boolean>
    * @Date 2023-05-11
    */
    @SLog
    @Post
    @Mapping("save")
    public R<Boolean> save(@Validated HistorderInput historderInput) {
    Historder historder = HistorderStruct.INSTANCE.toHistorder(historderInput);
        return super.save(historder);
    }

    /**
    * 修改
    * @param historderInput
    * @return R<Boolean>
    * @Date 2023-05-11
    */
    @SLog
    @Post
    @Mapping("update")
    public R<Boolean> update(@Validated HistorderInput historderInput) {
        Historder historder = HistorderStruct.INSTANCE.toHistorder(historderInput);
        return super.update(historder);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-05-11
    */
    @SLog
    @Post
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-05-11
    */
    @SLog
    @Post
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Historder.class, ids);
    }

    @SLog
    @Post
    @Mapping("startAndExecute")
    public R<Boolean> startAndExecute(@Validated HistorderInput historderInput) {
        Historder historder = HistorderStruct.INSTANCE.toHistorder(historderInput);
        baseService.startAndExecute(historder);
        return success();
    }
}
