package cn.angis.flow.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.flow.dto.input.ModelgroupInput;
import cn.angis.flow.dto.output.ModelgroupOutput;
import cn.angis.flow.dtomapper.ModelgroupStruct;
import cn.angis.flow.model.Modelgroup;
import cn.angis.flow.service.ModelgroupService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 模型分组前端控制器
* @author angis.cn
* @Date 2023-05-04
*/
@SLog("模型分组")
@Mapping("/flow/modelgroup/")
@Controller
public class ModelgroupController extends BaseController<ModelgroupService, Modelgroup> {

    /**
    * 分页查询
    * @param modelgroupInput
    * @param pageModel
    * @return R<PageResult<ModelgroupOutput>>
    * @Date 2023-05-04
    */
    @SLog
    @Get
    @Mapping("page")
    public R<PageResult<ModelgroupOutput>> page(ModelgroupInput modelgroupInput, PageModel pageModel) {
        Modelgroup modelgroup = ModelgroupStruct.INSTANCE.toModelgroup(modelgroupInput);
        PageResult<Modelgroup> pageResult = pageListByEntity(modelgroup, pageModel);
        List<ModelgroupOutput> modelgroupOutputList = ModelgroupStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, modelgroupOutputList));
    }

    /**
    * 保存
    * @param modelgroupInput
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("save")
    public R<Boolean> save(@Validated ModelgroupInput modelgroupInput) {
    Modelgroup modelgroup = ModelgroupStruct.INSTANCE.toModelgroup(modelgroupInput);
        return super.save(modelgroup);
    }

    /**
    * 修改
    * @param modelgroupInput
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("update")
    public R<Boolean> update(@Validated ModelgroupInput modelgroupInput) {
        Modelgroup modelgroup = ModelgroupStruct.INSTANCE.toModelgroup(modelgroupInput);
        return super.update(modelgroup);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Modelgroup.class, ids);
    }
}
