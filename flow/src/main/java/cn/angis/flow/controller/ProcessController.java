package cn.angis.flow.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.common.annotation.SLog;
import cn.angis.db.controller.BaseController;
import cn.angis.flow.dto.input.ProcessInput;
import cn.angis.flow.dto.output.ProcessOutput;
import cn.angis.flow.dtomapper.ProcessStruct;
import cn.angis.flow.model.Process;
import cn.angis.flow.service.ProcessService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 流程定义表前端控制器
* @author angis.cn
* @Date 2023-05-04
*/
@SLog("流程定义表")
@Mapping("/flow/process/")
@Controller
public class ProcessController extends BaseController<ProcessService, Process> {

    /**
    * 分页查询
    * @param processInput
    * @param pageModel
    * @return R<PageResult<ProcessOutput>>
    * @Date 2023-05-04
    */
    @SLog
    @Get
    @Mapping("page")
    public R<PageResult<ProcessOutput>> page(ProcessInput processInput, PageModel pageModel) {
        Process process = ProcessStruct.INSTANCE.toProcess(processInput);
        PageResult<Process> pageResult = pageListByEntity(process, pageModel);
        List<ProcessOutput> processOutputList = ProcessStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, processOutputList));
    }

    /**
    * 保存
    * @param processInput
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("save")
    public R<Boolean> save(@Validated ProcessInput processInput) {
    Process process = ProcessStruct.INSTANCE.toProcess(processInput);
        return super.save(process);
    }

    /**
    * 修改
    * @param processInput
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("update")
    public R<Boolean> update(@Validated ProcessInput processInput) {
        Process process = ProcessStruct.INSTANCE.toProcess(processInput);
        return super.update(process);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-05-04
    */
    @SLog
    @Post
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Process.class, ids);
    }
}
