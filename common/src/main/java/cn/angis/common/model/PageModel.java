package cn.angis.common.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author：chow
 * @Description: 分页类
 * @Date: 2020/11/26 8:33
 */
@Getter
@Setter
public class PageModel {

    /**
     * 当前页面：默认1
     */
    private long current = 1;

    /**
     * 每页显示条数：默认30
     */
    private int size = 30;

    /**
     * 排序字段
     */
    private String sort;

    /**
     * 排序类型：asc 或者 desc
     */
    private String order;
}
