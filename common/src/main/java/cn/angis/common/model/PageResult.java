package cn.angis.common.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 包名称：cn.angis.common.model
 * 类名称：PageResult
 * 类描述：分页信息返回结果
 * 创建人：@author angis.cn
 * 创建日期： 2022/12/21 21:00
 */
@Getter
@Setter
public class PageResult<T> {
    /**
     * 当前页
     */
    private long current;

    /**
     * 总页数
     */
    private long pages;

    /**
     * 每页显示条数
     */
    private long size;

    /**
     * 当前满足条件总行数
     */
    private long total;

    /**
     * 分布记录列表
     */
    private List<T> records;
}
