package cn.angis.common.config;

import cn.angis.common.constant.Crypto;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.asymmetric.SM2;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;


@Configuration
public class ConfigCommon {

    @Bean
    public SM2 sm2Init() {
        return SmUtil.sm2(Crypto.PRIVATEKEY, Crypto.PUBLICKEY);
    }

}




