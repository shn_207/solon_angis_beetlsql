package cn.angis.common.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @Author：chow
 * @Description:
 * @Date: 2021/9/13 16:14
 */
@AllArgsConstructor
@Getter
public enum ApiErrorCode implements IErrorCode {
    /**
     * 失败
     */
    ERROR(0, "操作失败"),
    /**
     * 成功
     */
    SUCCESS(1, "操作成功"),

    /**
     * 校验公共错误
     */
    CHECK_ERROR(1000, "校验公共错误"),
    OTHER_ERROR(3000, "其他exe 错误"),
    // token错误
    TOKEN_ERROR(2000, "token错误"),
    NO_TOKEN_ERROR(2001, "没有token，请重新登录"),
    USER_PASSWORD_ERROR(2002, "用户名和密码错误，请重新输入"),
    USER_UPDATE_PASS_ERROR(2005, "当前密码输入错误，请重新输入"),
    USER_UPDATE_PASS2_ERROR(2006, "两次密码输入不一致，请重新输入"),
    USER_LOCKED_ERROR(2003, "用户已锁定"),
    USER_NOROLE_LOCKED_ERROR(2004, "没有角色"),
    USER_NOPERMISSION_LOCKED_ERROR(2005, "没有权限"),
    USER_DISABLE(2007, "账号被封禁"),
    ;

    private final Integer code;
    private final String msg;

    public static ApiErrorCode fromCode(Integer code) {
        return Stream.of(ApiErrorCode.values())
                .filter(apiErrorCode -> apiErrorCode.code.equals(code))
                .findAny()
                .orElse(SUCCESS);
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return String.format(" ErrorCode:{code=%s, msg=%s} ", code, msg);
    }
}