package cn.angis.generate;


import cn.angis.generate.model.GenerModel;
import cn.angis.generate.option.GenOption;
import cn.angis.generate.util.GenCodeUtil;
import cn.angis.generate.util.GenUtils;
import com.github.yitter.contract.IdGeneratorOptions;
import com.github.yitter.idgen.YitIdHelper;
import org.beetl.sql.core.SQLManager;

public class GeneratorCode {

    private static final GenerModel generModel = GenUtils.getGenerByProps();

    private static String tplPath = "/generate/src/main/resources/templates/";


    public static void main(String[] args) {
        IdGeneratorOptions options = new IdGeneratorOptions((short) 5);
        options.SeqBitLength = 12;
        YitIdHelper.setIdGenerator(options);
        SQLManager sqlManager = GenCodeUtil.getDataSource(generModel.getDriverName(), generModel.getDbUrl(), generModel.getUsername(), generModel.getPassword());
        GenCodeUtil.initGroupTemplate(tplPath);

        // 指定要生成的内容
        GenOption genOption = new GenOption();
        genOption.setNeedEntity(true)
                .setNeedMapperAndController(true)
                .setNeedMenuSQL(true)
                .setNeedDBDoc(true);

        // 指定要生成代码的表名
        // 生成代码与文档
        GenCodeUtil.genCode(sqlManager, generModel.getParent(), generModel.getTableName(), generModel.getModule(), genOption);


//        System.out.println(Sm3.sm3("123456"));
//        for (int i = 0; i < 100; i++) {
//            System.out.println(YitIdHelper.nextId());
//        }
    }
}
