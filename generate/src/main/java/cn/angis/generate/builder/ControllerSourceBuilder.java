package cn.angis.generate.builder;

import cn.angis.generate.util.LowerFirstFnUtil;
import cn.hutool.core.date.DateUtil;
import org.beetl.core.Template;
import org.beetl.sql.gen.BaseProject;
import org.beetl.sql.gen.Entity;
import org.beetl.sql.gen.SourceConfig;
import org.beetl.sql.gen.simple.BaseTemplateSourceBuilder;

import java.io.Writer;
import java.util.Date;

public class ControllerSourceBuilder extends BaseTemplateSourceBuilder {
    public static String pojoPath = "controller.html";
    private String module;

    public ControllerSourceBuilder() {
        super("controller");
    }

    public ControllerSourceBuilder(String module) {
        super("controller");
        this.module = module;
    }

    public void generate(BaseProject project, SourceConfig config, Entity entity) {
        Template template = groupTemplate.getTemplate(pojoPath);
        // 注册方法
        groupTemplate.registerFunction("lowerFirst", new LowerFirstFnUtil());

        template.binding("className", entity.getName());
        template.binding("table", entity.getTableName());
        template.binding("package", project.getBasePackage(this.name));
        template.binding("module", this.module);
        template.binding("currentDate", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        template.binding("comment", entity.getComment());
        Writer writer = project.getWriterByName(this.name, entity.getName() + "Controller.java");
        template.renderTo(writer);
    }
}
