package cn.angis.generate.builder;

import cn.angis.generate.util.LowerFirstFnUtil;
import org.beetl.core.Template;
import org.beetl.sql.gen.BaseProject;
import org.beetl.sql.gen.Entity;
import org.beetl.sql.gen.SourceConfig;
import org.beetl.sql.gen.simple.BaseTemplateSourceBuilder;

import java.io.Writer;
import java.util.Arrays;

public class ServiceBuilder extends BaseTemplateSourceBuilder {
    public static String mapperPath = "service.html";
    private String module;

    public ServiceBuilder() {
        super("service");
    }

    public ServiceBuilder(String module) {
        super("service");
        this.module = module;
    }

    public void generate(BaseProject project, SourceConfig config, Entity entity) {
        Template template = groupTemplate.getTemplate(mapperPath);
        // 注册方法
        groupTemplate.registerFunction("lowerFirst", new LowerFirstFnUtil());

        template.binding("module", this.module);
        template.binding("className", entity.getName());
        template.binding("cols", "${cols}");
        template.binding("tableName", entity.getTableName());
        template.binding("package", project.getBasePackage(this.name));
        template.binding("comment", entity.getComment());
        template.binding("entityClass", entity.getName());
        String entityPkg = project.getBasePackage("entity");
        String mapperHead = entityPkg + ".*";
        template.binding("imports", Arrays.asList(mapperHead));
        Writer writer = project.getWriterByName(this.name, entity.getName() + "Service.java");
        template.renderTo(writer);
    }
}
