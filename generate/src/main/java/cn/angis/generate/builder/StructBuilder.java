package cn.angis.generate.builder;

import cn.angis.generate.util.LowerFirstFnUtil;
import org.beetl.core.Template;
import org.beetl.sql.gen.BaseProject;
import org.beetl.sql.gen.Entity;
import org.beetl.sql.gen.SourceConfig;
import org.beetl.sql.gen.simple.BaseTemplateSourceBuilder;

import java.io.Writer;
import java.util.Arrays;

public class StructBuilder extends BaseTemplateSourceBuilder {
    public static String mapperPath = "struct.html";
    private String module;
    private String suffix;

    public StructBuilder() {
        super("dtomapper");
        this.suffix = "Struct";
    }

    public StructBuilder(String module) {
        super("dtomapper");
        this.module = module;
    }

    public void generate(BaseProject project, SourceConfig config, Entity entity) {
        Template template = groupTemplate.getTemplate(mapperPath);
        // 注册方法
        groupTemplate.registerFunction("lowerFirst", new LowerFirstFnUtil());

        template.binding("module", this.module);
        template.binding("className", entity.getName());
        template.binding("cols", "${cols}");
        template.binding("tableName", entity.getTableName());
        template.binding("package", project.getBasePackage(this.name));
        template.binding("entityClass", entity.getName());
        template.binding("comment", entity.getComment());
        String entityPkg = project.getBasePackage("entity");
        String mapperHead = entityPkg + ".*";
        template.binding("imports", Arrays.asList(mapperHead));
        Writer writer = project.getWriterByName(this.name, entity.getName() + "Struct.java");
        template.renderTo(writer);
    }
}
