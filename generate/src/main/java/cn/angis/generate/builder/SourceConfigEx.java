package cn.angis.generate.builder;

import org.beetl.sql.clazz.ColDesc;
import org.beetl.sql.clazz.TableDesc;
import org.beetl.sql.clazz.kit.CaseInsensitiveHashMap;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.gen.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 包名称：cn.angis.generate.builder
 * 类名称：SourceConfigEx
 * 类描述：
 * 创建人：@author angis.cn
 * 创建日期： 2023/1/3 16:17
 */
public class SourceConfigEx extends SourceConfig {

    public SourceConfigEx(SQLManager sqlManager, boolean addDefault) {
        super(sqlManager, addDefault);
    }

    public SourceConfigEx(SQLManager sqlManager, List<SourceBuilder> sourceBuilder) {
        super(sqlManager, sourceBuilder);
    }

    public SourceConfigEx(SQLManager sqlManager, List<SourceBuilder> sourceBuilder, PreferDoubleType preferDoubleType, PreferDateType preferDateType) {
        super(sqlManager, sourceBuilder, preferDoubleType, preferDateType);
    }

    @Override
    protected Entity toEntity(TableDesc tableDesc, PackageList packageList) {
        Entity entity = new Entity();
        entity.setComment(tableDesc.getRemark());
        entity.setCatalog(tableDesc.getCatalog());
        entity.setTableName(tableDesc.getName());
        entity.setName(this.getSqlManager().getNc().getClassName(getClassName(tableDesc.getName())));
        ArrayList<Attribute> list = new ArrayList();
        CaseInsensitiveHashMap<String, ColDesc> cols = tableDesc.getColsDetail();
        Iterator var6 = cols.entrySet().iterator();

        while(var6.hasNext()) {
            Map.Entry colInfo = (Map.Entry)var6.next();
            ColDesc colDesc = (ColDesc)colInfo.getValue();
            Attribute attribute = this.toAttribute(tableDesc, colDesc, packageList);
            list.add(attribute);
        }

        entity.setList(list);
        packageList.getPkgs().add("org.beetl.sql.annotation.entity.*");
        return entity;
    }

    private String getClassName(String name) {
        if (name.indexOf("_")==-1) {
            return name;
        } else {
            String[] strs = name.split("_");
            String result = "";
            for (int i=1; i<strs.length; i++) {
                result += strs[i];
            }
            return result;
        }
    }

}
